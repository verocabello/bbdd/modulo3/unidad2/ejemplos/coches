<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coches".
 *
 * @property int $codigoCoche
 * @property string $marca
 * @property string $color
 *
 * @property Alquileres[] $alquileres
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['marca'], 'string', 'max' => 50],
            [['color'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoCoche' => 'Codigo Coche',
            'marca' => 'Marca',
            'color' => 'Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlquileres()
    {
        return $this->hasMany(Alquileres::className(), ['coche' => 'codigoCoche']);
    }
}
