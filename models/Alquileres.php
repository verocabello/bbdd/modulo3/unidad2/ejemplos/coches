<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alquileres".
 *
 * @property int $codigoAlquiler
 * @property int $usuario
 * @property int $coche
 * @property string $fecha
 *
 * @property Coches $coche0
 * @property Usuarios $usuario0
 */
class Alquileres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alquileres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'coche'], 'integer'],
            [['fecha'], 'safe'],
            [['usuario', 'coche', 'fecha'], 'unique', 'targetAttribute' => ['usuario', 'coche', 'fecha']],
            [['coche'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['coche' => 'codigoCoche']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario' => 'codigoUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoAlquiler' => 'Codigo Alquiler',
            'usuario' => 'Usuario',
            'coche' => 'Coche',
            'fecha' => 'Fecha de Alquiler',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoche0()
    {
        return $this->hasOne(Coches::className(), ['codigoCoche' => 'coche']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuarios::className(), ['codigoUsuario' => 'usuario']);
    }
    
    public function afterFind() {
        parent::afterFind();
        // los listados de fecha me salen en castellano
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)
                ->format("Y/m/d");
        return true;
    }
    
    public static function listar(){
        return self::find()->all();
    }
    
    public function getUsuarios(){
        $usuarios=\app\models\Usuarios::find()->all();
        $usuarios= \yii\helpers\ArrayHelper::map($usuarios,"codigoUsuario" , "nombre");
        return $usuarios;
    }
    
    public static function getUsuario(){
        $usuarios=\app\models\Usuarios::find()->all();
        $usuarios= \yii\helpers\ArrayHelper::map($usuarios,"codigoUsuario" , "nombre");
        return $usuarios;
    }
    
    public function getCoches(){
        $coches=\app\models\Coches::find()->select(["codigoCoche","CONCAT_WS(' ' ,codigoCoche,marca) marca"])->all(); 
        $coches= \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
        return $coches;
    }
    
    public static function getCoche(){
        $coches=\app\models\Coches::find()->select(["codigoCoche","CONCAT_WS(' ' ,codigoCoche,marca) marca"])->all(); 
        $coches= \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
        return $coches;
    }
    
    public static function getAlquileresusuario($id){
        return self::find()
                 ->where("usuario=$id");
    }
}
