<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alquileres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoAlquiler',
            'usuario',
            'usuario0.nombre',
            [
                'label'=>'Nombre del usuario',
                'format'=>'raw',
                'content'=>function($model){
                    return Html::a(
                            'Ver alquileres de ' . $model->usuario0->nombre,
                            [
                                'alquileres/alquileresusuarios',
                                'id'=>$model->usuario
                            ],
                            [
                                'class'=>'btn btn-primary'
                            ]
                    );
                }
            ],
            'coche',
            'coche0.marca',
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
