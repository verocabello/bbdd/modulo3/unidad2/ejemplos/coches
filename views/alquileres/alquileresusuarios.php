<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <div class="jumbotron">
        <h3>Estos son los alquileres de <?= $dataProvider->models[0]->usuario0->nombre ?> </h3>
        <p><?= Html::a(
                    "Volver a todos los alquileres",
                    ['alquileres/index'],
                    [
                        'class'=>'btn btn-primary'
                    ]
                ); ?>
        </p>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'codigoAlquiler',
            'coche',
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
