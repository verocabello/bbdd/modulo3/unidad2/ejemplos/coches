<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alquileres-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
    // Como metodo estatico
    // echo $form->field($model, 'usuario')->dropDownList(\app\models\Alquileres::getUsuario()); 
    ?>
    
    <?= $form->field($model, 'usuario')->dropDownList($model->usuarios) ?>
   
    <?= $form->field($model, 'coche')->dropDownList($model->coches); ?>
    
    <?php 
    // como metodo estatico
    // echo $form->field($model, 'coche')->dropDownList(\app\models\Alquileres::getCoche()); 
    ?>

        <?php    
        /* Opcion 1 utilizando active record para el label
        
        echo '<label class="control-label">' . $model->getAttributeLabel("fecha") . '</label>';
        echo DatePicker::widget([
            'model' => $model, 
            'attribute' => 'fecha',
            'options' => ['placeholder' => 'Enter birth date ...'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' => 'dd/mm/yyyy',
                'autoclose' => true,
            ]
        ]);
         */
        
        /* opcion 2, utilizando label directamente desde el DatePicker
         * 
         */
        
        echo $form->field($model, 'fecha')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Introduce fecha'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' => 'dd/mm/yyyy',
                'autoclose' => true,
            ]
        ]);
         
        
        ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
